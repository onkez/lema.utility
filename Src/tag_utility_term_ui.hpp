
#ifndef __TAG_UTILITY_TERM_UI_HPP__
#define __TAG_UTILITY_TERM_UI_HPP__

///********************************************************
/// The general Terminal UI Design for main Tui window:
///  _____________________________________
/// |                                     |
/// |Data to write or nbr of chars to read|
/// |_____________________________________|
///            ____    _____
///           |    |  |     |
///           |Read|  |Write|
///           |____|  |_____|
///
///********************************************************


#include <ncurses.h>
#include <cdk/cdk.h>
#include <string>
#include <map>
#include <thread>
#include <picc.hpp>

#define TUI_WIDTH 60
#define TUI_HEIGHT 10

using namespace std;

struct tsize
{
    int width;
    int height;
    tsize(): width(0), height(0){}
};
typedef tsize tsize_t;

struct tpos
{
    int x;
    int y;
    tpos(): x(0), y(0){}
};
typedef tpos tpos_t;

class IPiccFunctor
{
    public:
    
    virtual void operator ()(string, string) = 0;
};

///
/// @brief Main Terminal UI
///

class LemaTui
{
    public:
    
    LemaTui();
    ~LemaTui();
    
    static WINDOW *create_console_ui(tsize_t size, tpos_t pos, WINDOW *parent=NULL);
    static tsize_t* get_console_size();
    WINDOW* window();    
    tsize_t size();
    tpos_t position();
    int verbose_column();
    
    protected:
   
    int verbose_col;
    WINDOW *tui;
    tsize_t tui_size;
    tsize_t *cons_size;
    tpos_t tui_pos;
    
    void draw_tui_box();
};

///-----------------------------------------------------------------------------------------------------------------------------------
///-----------------------------------------------------------------------------------------------------------------------------------

#define LN_TUI_WIDTH 50
#define LN_TUI_HEIGHT 3

class LnRdWrPicc
{
    public:
    
    LnRdWrPicc(LemaTui& parnt, tsize_t size = tsize_t(), tpos_t pos = tpos_t());
    ~LnRdWrPicc();
    
    void read_line_edit(const string placeholder, IPiccFunctor *worker);
    tsize_t size();
    tpos_t position();
    
    private:
    
    CDKSCREEN *screen;
    CDKENTRY *LnRdWr;
    WINDOW *lntui;
    tsize_t lntui_size;
    tpos_t lntui_pos;
    tpos_t ln_txt_pos;
    LemaTui parent;
    int verbose_col;
    
    void draw_lntui_box();
    CDKENTRY* make_cdk_entry(const char* label, WINDOW *ww=NULL, tsize_t sz=tsize_t());
    CDKBUTTONBOX* make_button_box(CDKENTRY *e_widget, char **button_list);
    static int btnbox_call_back(
        EObjectType, void *,
        void *client_data, chtype key
    );
};

///-----------------------------------------------------------------------------------------------------------------------------------
///-----------------------------------------------------------------------------------------------------------------------------------

#define MAX_LABEL_MESSAGES 5

class PiccTui: public IPiccFunctor
{
    public:
    
    PiccTui();
    virtual ~PiccTui();
   
    virtual void operator ()(string button, string input);
    void operator << (string);
    
    void clean();
    
    private:
    
    Picc<PiccTui> picc;
    string read_button;
    string write_button;
    vector<string> messages;
    CDKSCREEN *screen;
    CDKLABEL *output_label;
    thread label_thread;
    
    void handle_read_results(long_t, byte_t*, dword_t);
    void handle_write_results(long_t);
    void label_func(string msg);
    bool is_a_number(string ss);
};


///-----------------------------------------------------------------------------------------------------------------------------------
///-----------------------------------------------------------------------------------------------------------------------------------




#endif
