
/// @file: pcsc_error.hpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

#ifndef __PCSC_ERROR_HPP__
#define __PCSC_ERROR_HPP__

#include <fstream>
#include <ctime>
#include <picc.hpp>

#define LOGGER_DIR_NAME ""
#define LOGGER_FILE_NAME "lema.log"

class PcscError
{
    public:
    
    static void log(long_t error_code=0, string error_msg="");
};

#endif
