
/// @file: tag_utility_term_ui.cpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

#include <tag_utility_term_ui.hpp>
#include <sys/ioctl.h>
#include <unistd.h>
#include <picc.cpp> // Templates

LemaTui::LemaTui()
{

    cons_size = nullptr;
    verbose_col = 2;
     
    tui_size.width = TUI_WIDTH;
    tui_size.height = TUI_HEIGHT;
    
    cons_size = get_console_size();
    
    tui_pos.x = (cons_size->width - tui_size.width)/2;
    tui_pos.y = (cons_size->height - tui_size.height)/2;
    
    tui = create_console_ui(tui_size, tui_pos);
    
    if(nullptr != tui)
    {
        keypad(tui, true);
    }
    
    refresh();
}

LemaTui::~LemaTui()
{
    if(nullptr != cons_size)
    {
        delete cons_size;
        cons_size = nullptr;
    }
    
    // End ncurses
    //endwin();
}


/// @brief It creates Terminal UI
///
/// @param size: The width and height of the console ui
/// @param pos: The coordinates (x, y) of the the console ui
/// @param parent: The parent ui if any.
WINDOW* LemaTui::create_console_ui(tsize_t size, tpos_t pos, WINDOW *parent)
{
    WINDOW *ui = nullptr;
    
    if(nullptr == parent)
    {
        ui = newwin(size.height, size.width, pos.y, pos.x);
    } 
    else 
    {
        ui = derwin(parent, size.height, size.width, pos.y, pos.x); 
    }
    
    refresh();
    
    return ui;
}

WINDOW* LemaTui::window()
{
    return tui;
}

tsize_t LemaTui::size()
{
    return tui_size;
}

tpos_t LemaTui::position()
{
    return tui_pos;
}

int LemaTui::verbose_column()
{
    return verbose_col;
}

void LemaTui::draw_tui_box()
{
    if(nullptr == tui)
    {
        verbose_col += 1;
        mvprintw(verbose_col, 1, "Error: tui is nullptr!\n");
        refresh();
        return;
    }
    
    box(tui, 0, 0);
    wrefresh(tui);
}

tsize_t* LemaTui::get_console_size()
{
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    
    tsize_t* csize = new tsize_t;
    
    csize->width = w.ws_col;
    csize->height = w.ws_row;
    
    return csize;
}

///-----------------------------------------------------------------------------------------------------------------------------------
///-----------------------------------------------------------------------------------------------------------------------------------

LnRdWrPicc::LnRdWrPicc(LemaTui& parnt, tsize_t size, tpos_t pos)
{
    lntui = nullptr;
    LnRdWr = nullptr;
    screen = nullptr;
    lntui_size = size;
    parent = parnt;
    verbose_col = parent.verbose_column();
    
    lntui_pos = pos; //(parent.size().width - lntui_size.width)/2;
                     //(parent.size().height - lntui_size.height)/2;
}

LnRdWrPicc::~LnRdWrPicc()
{
    if(nullptr != lntui)
    {
        delwin(lntui);
        lntui = nullptr;
    }
    
    if(nullptr != LnRdWr)
    {
        destroyCDKEntry(LnRdWr);
        LnRdWr = nullptr;
    }
    if(nullptr != screen
    ){
        destroyCDKScreen(screen); 
        screen = nullptr; 
    }
    
    endCDK();
}

void LnRdWrPicc::read_line_edit(const string placeholder, IPiccFunctor* worker)
{
    if(placeholder.empty())
    {
        verbose_col += 1;
        mvprintw(verbose_col, 1, "Error: no placeholder text given!\n");
        refresh();
        return;
    }
    
    start_ent:
    
    make_cdk_entry(placeholder.data());
    if(nullptr == LnRdWr)
    {
        verbose_col += 1;
        mvprintw(verbose_col, 1, "Error: failed to create entry widget!\n");
        refresh();
        return;
    }
    
    setCDKEntryLLChar (LnRdWr, ACS_LTEE);
    setCDKEntryLRChar (LnRdWr, ACS_RTEE);
    
    // Make Entry buttons
    const char *buttons = "Read Tag\nWrite On Tag\nEdit\nExit";
    char** button_list = CDKsplitString (buttons, '\n');
    
    CDKBUTTONBOX* btnbox = make_button_box(LnRdWr, button_list);
    
    refreshCDKScreen(screen);
    
    edit:
    
    // Activate Entry Widget
    char* data = activateCDKEntry(LnRdWr, nullptr);
    
    const char *msg[1];
    
    if(LnRdWr->exitType == vNORMAL)
    {
        char *input = copyChar(data);
        // Activate and Manage Buttons
        int selected = activateCDKButtonbox(btnbox, nullptr);
        
        if(btnbox->exitType == vNORMAL)
        {
            
            // Handle Exit and Edit  
            string button = string(button_list[selected]);           
            if(button == string("Edit"))
            {
                
                freeChar(input); input = nullptr;
                goto edit;
                
            } 
            else if(button == string("Exit"))
            {
                
                destroyCDKEntry(LnRdWr); LnRdWr = nullptr;
                destroyCDKButtonbox(btnbox); btnbox = nullptr;
                
                (*worker)(button, string(input));
                freeChar(input); input = nullptr;
                
                return;
                
            } 
            else 
            {
            
                // Handle other buttons activation
                destroyCDKEntry(LnRdWr); LnRdWr = nullptr;
                destroyCDKButtonbox(btnbox); btnbox = nullptr;
            
                (*worker)(button, string(input));
                freeChar(input); input = nullptr;
                
            }
            
        } 
        else if(btnbox->exitType == vESCAPE_HIT)
        {
            
            destroyCDKEntry(LnRdWr); LnRdWr = nullptr;
            destroyCDKButtonbox(btnbox); btnbox = nullptr;
            msg[0] = "Gracefully Exiting Button Box ...";
            popupLabel (screen, (CDK_CSTRING2) msg, 1);
            return;
            
        } 
        else 
        {
            
            destroyCDKEntry(LnRdWr); LnRdWr = nullptr;
            destroyCDKButtonbox(btnbox); btnbox = nullptr;
            msg[0] = "ButtonBox: What the hell happened?";
            popupLabel (screen, (CDK_CSTRING2) msg, 1);
            return;
            
        }
        
        if(nullptr != input)
        {
            freeChar(input); input = nullptr;
        }
        
    } 
    else if(LnRdWr->exitType == vESCAPE_HIT)
    {
        
        destroyCDKEntry(LnRdWr); LnRdWr = nullptr;
        destroyCDKButtonbox(btnbox); btnbox = nullptr;
        msg[0] = "Gracefully Exiting Entry Widget ...";
        popupLabel (screen, (CDK_CSTRING2) msg, 1);
        return;
        
    } 
    else 
    {
        
        destroyCDKEntry(LnRdWr); LnRdWr = nullptr;
        destroyCDKButtonbox(btnbox); btnbox = nullptr;
        msg[0] = "Entry Widget: What the hell happened?";
        popupLabel (screen, (CDK_CSTRING2) msg, 1);
        return;
        
    }
    
    CDKfreeStrings (button_list);
    if(nullptr != LnRdWr)
    {
        destroyCDKEntry(LnRdWr); LnRdWr = nullptr;
    }
    if(nullptr != btnbox)
    {
        destroyCDKButtonbox(btnbox); btnbox = nullptr;
    }
    if(nullptr != screen)
    {
        destroyCDKScreen(screen); 
        screen = nullptr; 
    }
    
    goto start_ent;
}

/// @brief Make CDK Entry Widget
///
/// @param label: Entry label or what would be entry placeholder
/// @param ww: Optional. NCurses window. If left out, parent's window will be used
/// @param sz: Size of the Entry widget. This is used to determine
///
/// @return: The instance variable LnRdWr will be returned, properly set to value. Check
///          whether it is not nullptr.
CDKENTRY* LnRdWrPicc::make_cdk_entry(const char *label, WINDOW *ww, tsize_t sz)
{
    WINDOW *w = nullptr == ww ? parent.window() : ww;
    tsize_t size = sz.width == 0 && sz.height == 0 ? lntui_size : sz;   
    
    if(nullptr == screen) screen = initCDKScreen(w);
    if(nullptr == LnRdWr)
    {
        LnRdWr = newCDKEntry(
            screen, CENTER, CENTER, label, "", 
            A_NORMAL, ' ', vMIXED, size.width, 0, size.width, true, false
        );
        initCDKColor();
    }
    
    return LnRdWr;
}

tsize_t LnRdWrPicc::size()
{
    return lntui_size;
}

tpos_t LnRdWrPicc::position()
{
    return lntui_pos;
}

void LnRdWrPicc::draw_lntui_box()
{
    if(nullptr == lntui)
    {
        verbose_col += 1;
        mvprintw(verbose_col, 1, "Error: lntui is nullptr!\n");
        refresh();
        return;
    }
    
    box(lntui, 0, 0);
    touchwin(parent.window());
    wrefresh(lntui);
}

CDKBUTTONBOX* LnRdWrPicc::make_button_box(CDKENTRY *e_widget, char **button_list)
{
    if(nullptr == e_widget || button_list == nullptr)
    {
        return nullptr;
    }
    
    CDKBUTTONBOX* btnbox = nullptr;
    bool drawbox = true;
    
    int btn_count = (int)CDKcountStrings ((CDK_CSTRING2)button_list);

    btnbox = newCDKButtonbox (
        screen, getbegx (e_widget->win),
	    (getbegy (e_widget->win) + e_widget->boxHeight - 1),
		1, e_widget->boxWidth - 1, 0, 1, btn_count,
	    (CDK_CSTRING2)button_list, btn_count,
		A_REVERSE, drawbox, FALSE
    );
      

    setCDKButtonboxULChar (btnbox, ACS_LTEE);
    setCDKButtonboxURChar (btnbox, ACS_RTEE);

    // Bind the some keys in the entry field to send a
    // them to the button box widget.
    
    //bindCDKObject (vENTRY, e_widget, KEY_TAB, btnbox_call_back, btnbox);
    //bindCDKObject (vENTRY, e_widget, KEY_RETURN, btnbox_call_back, btnbox);
    bindCDKObject (vENTRY, e_widget, CDK_NEXT, btnbox_call_back, btnbox);
    bindCDKObject (vENTRY, e_widget, CDK_PREV, btnbox_call_back, btnbox);
    
    char* CDK_WIDGET_COLOR = nullptr;
    if ((CDK_WIDGET_COLOR = getenv ("CDK_WIDGET_COLOR")) == nullptr) 
    {
        CDK_WIDGET_COLOR = nullptr;
    }
    setCDKButtonboxBackgroundColor (btnbox, CDK_WIDGET_COLOR);

    // Draw the button widget.
    drawCDKButtonbox (btnbox, drawbox);
    
    return btnbox;
}

int LnRdWrPicc::btnbox_call_back(
    EObjectType, void *,
    void *client_data, chtype key
){
    CDKBUTTONBOX *buttonbox = (CDKBUTTONBOX *)client_data;
    (void)injectCDKButtonbox (buttonbox, key);
    
    return (TRUE);
}

///-----------------------------------------------------------------------------------------------------------------------------------
///-----------------------------------------------------------------------------------------------------------------------------------

PiccTui::PiccTui()
{
    output_label = nullptr;
    picc.OutObject = this;
    read_button = "Read Tag";
    write_button = "Write On Tag";
    screen = initCDKScreen(nullptr);
    initCDKColor();
}

PiccTui::~PiccTui()
{
    clean();
}

void PiccTui::operator ()(string button, string input)
{
    
    if(button == read_button)
    {
        
        if(!is_a_number(input))
        {
            messages.clear();
            string msg = "In Read mode only an integer number is accepted!";
            messages.push_back(msg);
            msg = "To read the entire user memory, please input 0";
            messages.push_back(msg);
            msg = "<C>[Press Any key]";
            (*this) << msg;
            getch();
            messages.clear();
            return;
        }

        byte_t *data = nullptr;
        dword_t sz_read = (dword_t)std::stoi(input); 
        
        long_t rv = picc.Read(data, &sz_read);
        handle_read_results(rv, data, sz_read);
        
    }
    else if(button == write_button)
    {
        buffer_t buffer;
        buffer.data = (byte_t*)input.data();
        buffer.length = input.length();

        picc.DataToWrite(&buffer);
        long_t rv = picc.Write();
        handle_write_results(rv);
        
    } 
    else 
    {
        
        picc.clean();
        return;
        
    }
}

void PiccTui::operator << (string msg)
{  
    label_func(msg); 
}

void PiccTui::handle_read_results(long_t rv, byte_t* data, dword_t sz_read)
{
    if(SCARD_S_SUCCESS == rv)
    {
        Apdu apdu(data, sz_read);
        
        rv = apdu.bytes_to_string();
        if(rv != SCARD_S_SUCCESS)
        {
            PcscError::log(rv, "PiccTui::handle_read_results:bytes_to_string");
            return;
        }

        string s_data = apdu.string_data;

        apdu.clean();
        free(data); data = nullptr;
        
        string msg = string("Data: </B>") + s_data + string("<!B>");
        
        messages.clear();
        messages.push_back(msg);
        msg = "<C>[Press Any key]";
        (*this) << msg;
    } 
    else 
    {
        
        string msg = "</B>Failed to read from tag!<!B>";
        messages.clear();
        messages.push_back(msg);
        msg = "<C>[Press Any key]";
        
        (*this) << msg;
        
    }
    
    getch();
    messages.clear();
}

void PiccTui::handle_write_results(long_t rv)
{
    if(SCARD_S_SUCCESS == rv)
    {
        
        string msg = "</B>Data successfully written!<!B>";
        messages.clear();
        messages.push_back(msg);
        msg = "<C>[Press Any key]";
        
        (*this) << msg;
        
    } 
    else 
    {
        
        string msg = "</B>Data could not be written!<!B>";
        messages.clear();
        messages.push_back(msg);
        msg = "<C>[Press Any key]";
        
        (*this) << msg;
    }
    
    getch();
    messages.clear();
}

void PiccTui::clean()
{
    if(nullptr != output_label)
    {
        destroyCDKLabel(output_label);
        output_label = nullptr;
    }
    
    if(nullptr != screen)
    {
        destroyCDKScreen(screen); 
        screen = nullptr; 
    }
    picc.clean();
}

void PiccTui::label_func(string msg)
{
    if(messages.size() > MAX_LABEL_MESSAGES) messages.clear();
    
    // strip trailing line feed if any
    if(msg.at(msg.size()-1) == '\n') msg.erase(msg.size()-1, 1);
    messages.push_back(msg);
    
    size_t size = messages.size();
    
    char *message[size];
    
    int i =0;
    vector<string>::iterator it;
    for(it = messages.begin(); it != messages.end(); it++)
    {
        if(i>=(int)size) break;
        message[i] = (char*)it->c_str(); i++;
    }
    
    bool box = true;
    
    if(nullptr != output_label)
    { // Due to setCDKLabel not working
        destroyCDKLabel(output_label);
        output_label = nullptr;
    }
    
    if(nullptr == output_label)
    {
        
        output_label = newCDKLabel(
            screen, CENTER, CENTER, (CDK_CSTRING2)message, (int)size, box, false
        );
    } 
    else 
    { // This does not seem to work
        setCDKLabel(output_label, (CDK_CSTRING2)message, (int)size, box);
    }
    
    
    drawCDKLabel(output_label, box);
}

bool PiccTui::is_a_number(string ss)
{
    for(char c: ss)
    {
        if(!isdigit((unsigned char)c)) return false;
    }

    return true;
}

///-----------------------------------------------------------------------------------------------------------------------------------
///-----------------------------------------------------------------------------------------------------------------------------------


