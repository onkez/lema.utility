
/// @file: picc_atr.hpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

#ifndef __PICC_ATR_HPP__
#define __PICC_ATR_HPP__

#include <fstream>
#include <regex>
#include <vector>
#include <apdu.hpp>

#define SCARD_LIST_FILE_NAME ".smartcard_list.txt"
#define SCARD_LIST_FILE_URL "http://ludovic.rousseau.free.fr/softwares/pcsc-tools/smartcard_list.txt"
#define SCARD_LIST_FILE_DIR ""
#define MAX_PICC_NAME 256
#define MAX_TEXT_LINE 522

class PiccAtr
{
    public:
    
    string picc_atr;
    string picc_name_description;
    
    PiccAtr(string atr="");
    
    long_t find_picc_name_according_to_atr(string atr="");  

    private:
    
    std::vector<std::string> Split(std::string str, std::string sep);
    int UpdateCardListDatabase(string path);
    string ScardListDatabase();
};

#endif
