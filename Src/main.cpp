
/// @file: main.cpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)


#include <tag_utility_term_ui.hpp>

int main()
{
    LemaTui tui;
    tsize_t size; 
    PiccTui *picc = new PiccTui;
    
    size.width = LN_TUI_WIDTH; size.height = LN_TUI_HEIGHT;
    LnRdWrPicc RdWrLineEdit(tui, size);
    
    string placeholder = "Data to write or nbr of characters to read";
    RdWrLineEdit.read_line_edit(placeholder, picc);
    
    delete picc;
    
    //getch();
    
    return 0;
}
