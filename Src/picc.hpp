
/// @file: picc.hpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

#ifndef __PICC_HPP__
#define __PICC_HPP__

#include <apdu.hpp>
#include <iostream>
#include <mutex>

#define ERROR_INVALID_PARAMETER SCARD_E_INVALID_PARAMETER
#define ERROR_UNKNOWN_PROTOCOL  0xF00F
#define ERROR_INVALID_PICC_DATA 0xFF0F
#define ERROR_FORMATTING_DATA   0xF000
#define SCARD_FILE_OPEN_ERROR   0xF0C0
#define ERROR_GENERAL_ERROR     0x0000
#define SCARD_S_OK              0x9000
#define SCARD_E_ERROR           0x6300
#define SCARD_E_REGEX           0xF00D
#define SCARD_ERROR_CODE_LENGTH 0x02
#define SCARD_MAX_RECV_BUFFER_SIZE (256+2)
#define NUM_NIBBLES_IN_BYTE 0x02
#define TIMEOUT 2000
using namespace std;

typedef DWORD dword_t;
typedef BYTE byte_t;
typedef LONG long_t;
typedef bool bool_t;

typedef struct eeprom_features 
{
    dword_t start_block_number;
    dword_t end_block_number;
    dword_t block_size;
    dword_t read_block_size;
    dword_t write_block_size;
    dword_t user_memory_size;
}
EEPROM_FEATURES, *LP_EEPROM_FEATURES;

typedef struct PiccIo
{
    byte_t* read_data;
    dword_t length_read_data;
    string data_to_write;
}
PiccIoData, *LpPiccIoData;

typedef struct _buffer
{
    byte_t* data;
    dword_t length;
}
buffer_t, *lp_buffer_t;

template<class OutStream>
class Picc
{
public:
    
    byte_t *recv_buffer;
    dword_t recv_buffer_len;
    SCARDCONTEXT h_context;
    string pcd_reader_name;
    string picc_atr;
    bool_t plug_n_play;
    vector<APDU_DATA> used_apdus;
    vector<APDU_DATA> received_data;
    EEPROM_FEATURES mifare_ultralight_fts;
    OutStream *OutObject;
    
    Picc();
    ~Picc();
    long_t Read(byte_t *&data, dword_t *sz_read);
    long_t Write();
    long_t SendApdus(vector<APDU_DATA> apdus);
    void clean();

    buffer_t* DataToWrite();
    void DataToWrite(buffer_t* dt);
    
protected:
    
    long_t make_read_apdus(LP_EEPROM_FEATURES features, dword_t sz_read=0);
    long_t make_write_apdus(LP_EEPROM_FEATURES features, byte_t* data, dword_t sz_data);
    long_t make_read_write_apdus(
        LP_EEPROM_FEATURES features, byte_t* data,
        dword_t sz_data, bool_t bl_write=true, dword_t sz_read=0
    );
    long_t wait_for_reader_and_card();
    long_t scan_for_pcd();
    long_t transmit_apdus(vector<APDU_DATA> apdus = vector<APDU_DATA>());
    dword_t get_apdu_error_code(byte_t* resp, dword_t resp_len);
    bool_t is_mifare_ultralight(string atr);
    long_t assemble_read_data(byte_t *&data, dword_t *length);
    void verbose(string out);
    
    string const_read_apdu;
    string const_write_apdu;

private:

    string OldAtr();
    void OldAtr(string atr);
    string NewAtr();
    void NewAtr(string atr);

    string old_atr;
    mutex old_atr_mtx;
    string new_atr;
    mutex new_atr_mtx;
    buffer_t* data_to_write;
    mutex data_to_write_mtx;
};

#endif
