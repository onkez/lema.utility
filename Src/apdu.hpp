
/// @file: apdu.hpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

#ifndef __APDU_HPP__
#define __APDU_HPP__

#include <stdlib.h>
#include <string.h>
#include <PCSC/winscard.h>
#include <string>
#include <cmath>
#include <vector>
#include <cctype>
#include <regex>

using namespace std;

typedef DWORD dword_t;
typedef BYTE byte_t;
typedef LONG long_t;
typedef bool bool_t;

///
/// @brief Write/Read APDU structure needed for transactions
///
typedef struct apdu_data
{
    string command;
    byte_t* data; // APDU payload data
    byte_t *bin_data;
    dword_t bin_data_length;
    dword_t data_length; // size of payload data
    
    apdu_data():
        command(""),
        data(nullptr),
        bin_data(nullptr),
        bin_data_length(0),
        data_length(0)
    {}
} APDU_DATA, *LP_APDU_DATA;

class Apdu
{
    public:
    
    byte_t *bytes_data;
    dword_t bytes_data_length;
    string string_data;
    bool_t is_hex_string;
    
    Apdu(byte_t *bytes=NULL, dword_t bytes_len=0, string chars="", bool_t b_hex_string=true);
    ~Apdu();
    static bool_t does_string_occur(string tgtstr, string keystr, bool_t case_ins=true);
    long_t make_apdu(string command, byte_t* data=nullptr, dword_t length=0);
    long_t apdustring_to_bytes();
    long_t string_to_bytes();
    long_t to_bytes();
    long_t bytes_to_hexstring();
    long_t bytes_to_string(bool_t b_hex_string=false);
    void clean();
};

#endif
