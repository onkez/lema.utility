
/// @file: apdu.cpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

///
///    @brief Converts APDU and PICC responses to human readable values.
///    
///    Note that APDU are made of arrays of bytes. Each byte is made of
///    two nibbles each representing a hexadecimal value from 0 to F, that is,
///    0,1,2,3,4,5,6,7,8,9,A(a),B(b),C(c),D(d),E(e),F(f).
///    Unless a specific meaning is assigned to a value to or from a PICC, these
///    values must be humanly represented as hexadecimal nibbles.
///    
///    E.g "ff00010c" is {0xff, 0x00, 0x01, 0x0c} while the buffer made of these
///        binary bits (11111111000000000000000100001100) will be humanly rendered
///        as "ff00010c" or as the array {0xff, 0x00, 0x01, 0x0c}
///

#include <apdu.hpp>
#include <pcsc_error.hpp>

Apdu::Apdu(byte_t *bytes, dword_t bytes_len, string chars, bool_t b_hex_string)
{
    if(bytes != nullptr && bytes_len > 0)
    {
        bytes_data = (byte_t*)malloc(bytes_len);
        if(bytes_data)
        {
            memcpy(bytes_data, bytes, bytes_len);
        } 
        else 
        {
            bytes_data = bytes;
        }
    } 
    else 
    {
        bytes_data = bytes;
    }
    string_data = chars;
    bytes_data_length = bytes_len;
    is_hex_string = b_hex_string;
}
    
Apdu::~Apdu()
{
    if(bytes_data != nullptr)
    {
        free(bytes_data); bytes_data = nullptr; bytes_data_length = 0;
    }
}
    
///
///    @brief Converts an APDU string to an array of bytes
///        
///    The string is a string of hexadecimals, so we interprete each character
///    as a nibble so that each byte is made of two characters.
///
    
long_t Apdu::apdustring_to_bytes()
{
    long_t rv;
    string::size_type len, bytes_num, nibbles_num = NUM_NIBBLES_IN_BYTE;
    
    // Free possibly allocated memory
    if(bytes_data != nullptr)
    {
        free(bytes_data); bytes_data = nullptr; bytes_data_length = 0;
    }
        
    if (string_data.empty())
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Apdu::apdustring_to_bytes():string_data.empty()");
        return rv;
    }
    len = string_data.size();
    if((len%nibbles_num) != 0)
    {
        rv = ERROR_INVALID_PICC_DATA;
        PcscError::log(rv, "Apdu::apdustring_to_bytes():len%%nibbles_num");
        return rv;
    }
    bytes_num = len/nibbles_num;
    bytes_data_length = bytes_num;
        
    bytes_data = (byte_t*)malloc(bytes_data_length);
    if(bytes_data == nullptr)
    {
        rv = SCARD_E_INSUFFICIENT_BUFFER;
        PcscError::log(rv, "Apdu::apdustring_to_bytes():malloc()");
        return rv;
    }
    
    int byte_count = 0;
        
    for(int i=0; i<(int)len; i++)
    {
        // Grubbing the nibbles that form a byte
        string nibbles;
        int j = i, k = 0, exp = 0;
        for(j=i; j<(int)(i+nibbles_num); j++)
        {
            nibbles.push_back(string_data.at(j));
        }
        i = j-1; 
            
        // Converting the two nibbles into a decimal number
        exp = nibbles_num;
        byte_t byt = 0x00;
        for(k=0; k<(int)nibbles_num; k++)
        {
            exp -= 1;
            if(nibbles.at(k) == '0')
            {
                byt += 0 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '1') 
            {
                byt += 1 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '2') 
            {
                byt += 2 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '3') 
            {
                byt += 3 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '4') 
            {
                byt += 4 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '5') 
            {
                byt += 5 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '6') 
            {
                byt += 6 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '7') 
            {
                byt += 7 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '8') 
            {
                byt += 8 * (pow(16, exp));
            } 
            else if (nibbles.at(k) == '9') 
            {
                byt += 9 * (pow(16, exp));
            } 
            else if ((nibbles.at(k) == 'A') || (nibbles.at(k) == 'a')) 
            {
                byt += 10 * (pow(16, exp));
            } 
            else if ((nibbles.at(k) == 'B') || (nibbles.at(k) == 'b')) 
            {
                byt += 11 * (pow(16, exp));
            } 
            else if ((nibbles.at(k) == 'C') || (nibbles.at(k) == 'c')) 
            {
                byt += 12 * (pow(16, exp));
            } 
            else if ((nibbles.at(k) == 'D') || (nibbles.at(k) == 'd')) 
            {
                byt += 13 * (pow(16, exp));
            } 
            else if ((nibbles.at(k) == 'E') || (nibbles.at(k) == 'e')) 
            {
                byt += 14 * (pow(16, exp));
            } 
            else if ((nibbles.at(k) == 'F') || (nibbles.at(k) == 'f')) 
            {
                byt += 15 * (pow(16, exp));
            }                
        }
          
        memcpy(bytes_data+byte_count, &byt, sizeof(byte_t));
        byte_count += 1;
    }
        
    return SCARD_S_SUCCESS;
}
    
///
///    @brief Converts a string into an array of bytes
///        
///    The string will just be binarized by dumping its equivalent
///    stream of bits.
///    This method is typically used to handle non-apdu-like data before
///    writing it to PICC.
///        
long_t Apdu::string_to_bytes()
{
    long_t rv;
    
    // Free possibly allocated memory
    if(bytes_data != nullptr)
    {
        free(bytes_data); bytes_data = nullptr; bytes_data_length = 0;
    }
        
    if(string_data.empty())
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Apdu::string_to_bytes:string_data.empty");
        return rv;
    }
        
    bytes_data_length = string_data.size();
        
    bytes_data = (byte_t*)malloc(bytes_data_length);
    if(bytes_data == nullptr)
    {
        rv = SCARD_E_INSUFFICIENT_BUFFER;
        PcscError::log(rv, "Apdu::string_to_bytes:malloc");
        return rv;
    }
        
    memcpy(bytes_data, string_data.data(), bytes_data_length);
        
    return SCARD_S_SUCCESS;
}
    
///
///    @brief Converts a stream of bits to a human-readable string of hexadecimals.
///

long_t Apdu::bytes_to_hexstring()
{
    return bytes_to_string(true);
}

///
///    @brief Converts a stream of bits to a human-readable string of ascii or hexadecimals
///    
///    The @param b_hex_string is defaulted to false, meaning that the bytes will be
///    converted to an ASCII string. If set to faulse, the function dump the binaries
///    in a hex-stringified format.
///  
long_t Apdu::bytes_to_string(bool_t b_hex_string)
{
    long_t rv;
        
    if(bytes_data == nullptr || bytes_data_length == 0)
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Apdu::bytes_to_string:invalid_parameter");
        return rv;
    }
    
    is_hex_string = b_hex_string;
    
    // Clear string data
    string_data.clear();
    
    for(int i=0; i<(int)bytes_data_length; i++)
    {
        char ss[3] = {'\0', '\0', '\0'};
        int n = 0;
        if(is_hex_string)
        {
            n = sprintf(ss, "%.2X", *(bytes_data+i));
            if(n<0){
                rv = ERROR_FORMATTING_DATA;
                PcscError::log(rv, "Apdu::bytes_to_string:sprintf");
                return rv;
            }
        } 
        else 
        {
            n = sprintf(ss, "%c", (const char)*(bytes_data+i));
            if(n<0)
            {
                rv = ERROR_FORMATTING_DATA;
                PcscError::log(rv, "Apdu::bytes_to_string:sprintf");
                return rv;
            }
        }
        string s_ss((const char*)ss);
        string_data.append(s_ss);
    }
        
    return SCARD_S_SUCCESS;
}

///
///    @brief Convert string to bytes according to its meaningful format
///    
///    The meaningful format of a string is: a string of hexadecimals or
///    an ordinary string of characters.
///
long_t Apdu::to_bytes()
{
    long_t rv;
    
    if(is_hex_string)
    { // If the data is in APDU format, i.e., hex digits
        rv = apdustring_to_bytes();
        if(rv != SCARD_S_SUCCESS)
        {
            PcscError::log(rv, "Apdu::to_bytes:apdustring_to_bytes");
            return rv;
        }
    } 
    else 
    { // If data is just an ordinary string
        rv = string_to_bytes();
        if(rv != SCARD_S_SUCCESS)
        {
            PcscError::log(rv, "Apdu::to_bytes:string_to_bytes");
            return rv;
        }
    }
    
    return rv;
}

///
///    @brief Format APDU according to ISO/IEC 14443.
///    
///    APDU must be a string of hexadecimals from start to end.
///    Here, @param @b data is assumed to be an ordinary string of data while
///    @param @b command is a string of hexadecimals making up the command part
///    of an APDU.
///    
///    If @param data is empty, then the APDU will hold only the command.
///    @param length: Length of data, if any.
///    
///    The ultimate APDU will be stored in the instance variable @b bytes_data
///    and its length @b bytes_data_length, whereas the instance variable 
///    @b string_data will hold its stringified form.
///
long_t Apdu::make_apdu(string command, byte_t *data, dword_t length)
{
    long_t rv;
    byte_t *bin_command = nullptr, *bin_data = nullptr;
    dword_t len_cmd = 0, len_data = 0;
    
    // Validate the parameters
    if(command.empty())
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Apdu::make_apdu:command.empty");
        return rv;
    }
    
    // Binarize the command part
    clean();
    
    string_data = command;
    is_hex_string = true;
    rv = to_bytes();
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Apdu::make_apdu:to_bytes");
        return rv;
    }
    
    len_cmd = bytes_data_length;
    bin_command = (byte_t*)malloc(len_cmd);
    if(bin_command == nullptr)
    {
        rv = SCARD_E_INSUFFICIENT_BUFFER;
        PcscError::log(rv, "Apdu::make_apdu:malloc");
        return rv;
    }
    
    memcpy(bin_command, bytes_data, len_cmd);
    clean();
    
    // Make the APDU
    
    bytes_data_length = len_cmd + length;
    bytes_data = (byte_t*)malloc(bytes_data_length);
    if(bytes_data == nullptr)
    {
        rv = SCARD_E_INSUFFICIENT_BUFFER;
        PcscError::log(rv, "Apdu::make_apdu:malloc");
        return rv;
    }
    
    memcpy(bytes_data, bin_command, len_cmd);
    if(data != nullptr) memcpy(bytes_data+len_cmd, data, length);
    
    if(bin_command != nullptr)
    { 
        free(bin_command); bin_command = nullptr;
    }
    
    ///*************************************************************************
    ///    Store entire APDU as a hex-string, so turn data into hex-string first
    ///*************************************************************************
    if(nullptr != data)
    {
        byte_t *apdu_bin_data = bytes_data; // Saving our APDU binaries
        dword_t apdu_bin_data_length = bytes_data_length; // Saving the length of our APDU binaries
    
        // Now proceed and turn data part into hex-string
        bytes_data = data;
        bytes_data_length = length;
    
        rv = bytes_to_hexstring();
        if(rv != SCARD_S_SUCCESS)
        {
            PcscError::log(rv, "Apdu::make_apdu:bytes_to_hexstring");
            return rv;
        }
        // Store string data
        string s_data; s_data.assign(string_data); 
        bytes_data = nullptr; // Since it was not allocated as memory, to avoid free() exception
        clean();
    
        // Make our APDU string now
        string_data = command + s_data;
        // Restore APDU binaries variables
        bytes_data = apdu_bin_data;
        bytes_data_length = apdu_bin_data_length;
    } 
    else 
    {
        string_data = command;
    }
    ///*************************************************************************
    ///    End
    ///*************************************************************************
    
    rv = SCARD_S_SUCCESS;
    
    return rv;
}

///
///    @brief Finds occurrence of a string in a given string
///    
///    Note that the search is case insensitive by default. We do not use
///    the regular expression facility because the key string would be bound
///    with restrictions associated with regular expressions, such as, escaping
///    special characters.
///
bool_t Apdu::does_string_occur(string tgtstr, string keystr, bool_t case_ins)
{
    bool_t bl = false;
    
    if(tgtstr.empty()) return bl;
    if(keystr.empty()) return bl;
    
    string::size_type tgtsz = tgtstr.size(), keysz = keystr.size();
    string::iterator itr;
    
    string keymngd; keymngd.assign(keystr);
    if(case_ins)
    {
        for(itr = keymngd.begin(); itr != keymngd.end(); itr++)
        {
            *itr = tolower(*itr);
        }
    }
    
    for(int i=0; i<(int)(tgtsz-keysz+1); i++)
    {
        string sub = tgtstr.substr(i, keysz);
        if(case_ins)
        {
            for(itr = sub.begin(); itr != sub.end(); itr++)
            {
                *itr = tolower(*itr);
            }
        }
        
        if(!sub.compare(keymngd)){ bl = true; break; }
    }
    
    return bl;
}

void Apdu::clean()
{
    string_data.clear();
    if(bytes_data != nullptr)
    {
        free(bytes_data); bytes_data = nullptr; bytes_data_length = 0;
    }
}

