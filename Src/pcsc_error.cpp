
/// @file: pcsc_error.cpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

#include <pcsc_error.hpp>

void PcscError::log(long_t error_code, string error_msg)
{
    ofstream logger;
    string logger_file_name = string(LOGGER_DIR_NAME) + "/" + string(LOGGER_FILE_NAME);
    
    logger.open(logger_file_name.c_str(), ios::app);
    if(!logger.is_open())
    {
        logger_file_name = LOGGER_FILE_NAME; 
        logger.open(logger_file_name.c_str(), ios::app);
        if(!logger.is_open()) return;
    }
    time_t t = time(nullptr);
    string date(asctime(localtime(&t))); 
    date.erase(date.end()-1); // Remove the trailing new line character
    
    dword_t len = sizeof(long_t) + error_msg.size()*sizeof(char) + date.size()*sizeof(char) + 16;
    
    char *msg_buffer = (char*)malloc(len);
    if(nullptr == msg_buffer){ logger.close(); return; }
    
    int n = sprintf(msg_buffer, "\n%s: [0x%lX] %s", date.c_str(), error_code, error_msg.c_str());
    if(n<0)
    {
        if(nullptr != msg_buffer){ free(msg_buffer); msg_buffer = nullptr; }
        logger.close();
        return;
    }
    
    logger << string(msg_buffer);
    
    if(nullptr != msg_buffer){ free(msg_buffer); msg_buffer = nullptr; }
    
    logger.close();
}
