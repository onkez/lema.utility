
/// @file: picc_atr.cpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)

#include <picc_atr.hpp>
#include <pcsc_error.hpp>
#include <boost/filesystem.hpp>

PiccAtr::PiccAtr(string atr):
    picc_atr(atr),
    picc_name_description("")
{}

std::vector<std::string> PiccAtr::Split(std::string str, std::string sep)
{
    char* cstr=const_cast<char*>(str.c_str());
    char* current;
    std::vector<std::string> arr;
    
    current = strtok(cstr, sep.c_str());
    
    while(current != nullptr)
    {
        arr.push_back(current);
        current = strtok(nullptr, sep.c_str());
    }
    
    return arr;
}

///
/// @brief Finds or creates the card list database file
///
string PiccAtr::ScardListDatabase()
{
    string database_path;
    
    string database_file_name = SCARD_LIST_FILE_NAME;
    string home = std::getenv("HOME");
    
    string f1 = string("/usr/local/Lema/") + database_file_name;
    string f2 = string("/usr/local/share/Lema/") + database_file_name;
    string f3 = string("/usr/share/Lema/") + database_file_name;
    string f4 = home + string("/.Lema/") + database_file_name;
    
    string paths[4] = {f1, f2, f3, f4};
    
    for(string s: paths)
    {
        ifstream file(s);
        if(file.is_open())
        {
            file.close();
            database_path.assign(s);
            break;
        }
        else
        {
            vector<string> dirsegs = Split(s, "/");
            string dir = "/";
            auto limit = dirsegs.size()-1;
            
            for(int i=0; i<limit; i++)
            {
                if(i < limit-1)
                {
                    dir += dirsegs[i] + string("/");
                }
                else
                {
                    dir += dirsegs[i];
                }
            }
            
            //PcscError::log(0, dir);
            
            boost::system::error_code ec;
            if(boost::filesystem::create_directories(dir, ec))
            {
                database_path.assign(s);
                break;
            }
            else
            {
                ifstream f(s);
                if(f.is_open())
                {
                    f.close();
                    database_path.assign(s);
                    break;
                }
            }
        }
    }
    
    // If possible, update the database file
    UpdateCardListDatabase(database_path);
    
    return database_path;
}

int PiccAtr::UpdateCardListDatabase(string path)
{
    string url = string(SCARD_LIST_FILE_URL);
    string cmd = string("wget --quiet --output-document=") + path + string(" ") + url;
    
    return system(cmd.c_str());
}

///
/// @brief Uses a generic ATR list in a file to find PICC name
///    
/// It searches the file in the current directory first, if it cannot find it,
/// then it searches in a Macro-defined directory. This file has a standardized
/// format which is assumed in processing it:
///    
///    syntax:
///        ATR in regular expression form
///        \t descriptive text
///        \t descriptive text
///        \t descriptive text
///        empty line
///       
///    
/// The original ATR list can be found at <a href="http://ludovic.rousseau.free.fr/softwares/pcsc-tools/smartcard_list.txt"> 
/// Original Generic ATR list </a>.
///    
/// Figuring out which PICC we are dealing with is an important step in processing it.
/// Such features as EEPROM design are crucial in read/write procedures.
///
long_t PiccAtr::find_picc_name_according_to_atr(string atr)
{
    long_t rv;
    
    // Validate parameters
    if(!atr.empty()) picc_atr.assign(atr);
    if(picc_atr.empty())
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "PiccAtr::find_picc_name_according_to_atr:invalid_parameter");
        return rv;
    }
     
    string card_list_file = ScardListDatabase();
    ifstream scard_list(card_list_file);
    
    if(!scard_list.is_open())
    {
        rv = SCARD_FILE_OPEN_ERROR;
        PcscError::log(rv, "PiccAtr::find_picc_name_according_to_atr:ifstream");
        return rv;
    }
    
    dword_t ref_atr_size = MAX_TEXT_LINE;
    char *ref_atr = (char*)malloc(ref_atr_size);
    if(ref_atr == nullptr)
    {
        rv = SCARD_E_INSUFFICIENT_BUFFER;
        PcscError::log();
        return rv;
    }

    ///
    /// FIXME: The regex engine works improperly on Raspberry Pi.
    /// So we test it for now, although it is very costly, before
    /// going on. The structure of the file requires regular expressions,
    /// so let us not just give up all the power of regex.
    ///

    // Begin Regex Test

    bool_t regex_works = false;
    try 
    {
        regex reg("Well, [123] (..its\\.\\.)+");
        string ss = "Well, 2 suits..suits.. and so on";
        if(regex_search(ss, reg)) regex_works = true;
    } 
    catch(...)
    {
        regex_works = false;
    }

    if(regex_works) printf("[\033[32mUsing Regex Engine\033[0m]\n");
    else printf("[\033[31mDiscarding Regex Engine\033[m]\n");

    // End Regex Test 
    
    int line = 0;
    while(scard_list)
    {
        memset(ref_atr, '\0', ref_atr_size);
        scard_list.getline(ref_atr, ref_atr_size);
        if(scard_list)
        {
            line += 1;
            
            size_t len = strlen(ref_atr), actual_len = 0;
            if(len == 0) continue;
            // Skip lines that do not contain ATR
            if(ref_atr[0] == '#' || ref_atr[0] == '\t') continue;
            
            string s_ref_atr((const char*)ref_atr), s_ss;
            // Remove spaces from reference ATR
            string::iterator ref_it;
            for(ref_it = s_ref_atr.begin(); ref_it != s_ref_atr.end(); ref_it++)
            {
                if(*ref_it != ' '){
                    s_ss.push_back(*ref_it);
                }
            }
            s_ref_atr.clear(); s_ref_atr.assign(s_ss);
            
            actual_len = s_ref_atr.size();
            // reference ATR must be less or equal to PICC ATR.
            // if(actual_len > picc_atr.size()) continue;

            //printf("%d: %s\n", line, s_ref_atr.c_str());

            if(regex_works)
            {

                try 
                {
                    regex atr_regex(s_ref_atr);
                    if(!regex_search(picc_atr, atr_regex)) continue;
                } 
                catch (...)
                {
                    rv = SCARD_E_REGEX;
                    string e_ss =
                        string("PiccAtr::find_picc_name_according_to_atr:regex[") +
                        s_ref_atr +
                        string("]");

                    PcscError::log(rv, e_ss.c_str());
                    goto lets_go;
                }

            } 
            else 
            {

                // Legacy Implementation that does not use Regular Expression facility
                // Fill the regex '.' with an actual character from PICC ATR
                for(int i=0; i<(int)actual_len; i++)
                {
                    if(s_ref_atr.at(i) == '.')
                    {
                        if(i < (int)picc_atr.size()){
                            s_ref_atr[i] = picc_atr.at(i);
                        }
                    }
                }

                ///******************************************************************************
                ///    Compare reference ATR with the ref-ATR-length first characters in PICC ATR.
                ///******************************************************************************
                string sub_atr = picc_atr.substr(0, actual_len);

                if(s_ref_atr.compare(sub_atr)) continue;
                ///*******************************************************************************
                ///    End Compare
                ///*******************************************************************************
            }
            
            // They are the same, so get PICC name
            dword_t nsize = MAX_PICC_NAME;
            char *p_name = (char*)malloc(nsize);
            if(nullptr == p_name)
            {
                rv = SCARD_E_INSUFFICIENT_BUFFER;
                PcscError::log(rv, "PiccAtr::find_picc_name_according_to_atr:malloc");
                goto lets_go;
            }
            string picc_name_desc;
            // Read lines until we hit empty line
            while(scard_list)
            {
                memset(p_name, '\0', nsize);
                scard_list.getline(p_name, nsize);
                if(!scard_list) break;
                size_t plen = strlen(p_name);
                if(plen == 0) break; // We've hit an empty line
                picc_name_desc.append(string((const char*)p_name));
            }
            
            if(p_name != nullptr){ free(p_name); p_name = nullptr; }
            
            // If ATR list is formatted according to the above syntax, 
            // we have got the PICC name description
            picc_name_desc.erase(picc_name_desc.begin());// Remove the leading tab
            picc_name_description.assign(picc_name_desc);
            break;
        }    
    }
    
    rv = SCARD_S_SUCCESS;

    lets_go:

    scard_list.close();
    if(ref_atr != nullptr){ free(ref_atr); ref_atr = nullptr; }
    
    return rv; 
}
