
/// @file: picc.cpp
/// @author: Obed-Edom Nkezabahizi (onkezabahizi@gmail.com)


///
///    The PICC must be PC/SC compliant. Note that the following packages must be present
///    on your system: @b libpcsclite, and their dependencies.
///    
///    @brief This class encapsulates a Proximity Integrated Circuit Card (PICC)
///

#ifndef __PICC_CXX__
#define __PICC_CXX__

#include <picc.hpp>
#include <pcsc_error.hpp>
#include <picc_atr.hpp>
#include <memory>

template<class OutStream>
Picc<OutStream>::Picc()
{
    recv_buffer = nullptr; 
    recv_buffer_len = SCARD_MAX_RECV_BUFFER_SIZE;
    h_context = 0;
    pcd_reader_name = "";
    plug_n_play = true;
    const_write_apdu = "FFD600";
    const_read_apdu = "FFB000";
    used_apdus = vector<APDU_DATA>();
    received_data = vector<APDU_DATA>();
    OutObject = nullptr;
    DataToWrite(nullptr);
    ///
    /// Mifare Ultralight memory features
    /// <a href="http://www.orangetags.com/wp-content/downloads/datasheet/NXP/MF0ICU1.pdf"> See </a>
    ///
    mifare_ultralight_fts.start_block_number = 0x04;
    mifare_ultralight_fts.end_block_number = 0x0F;
    mifare_ultralight_fts.block_size = 0x04;
    mifare_ultralight_fts.read_block_size = 0x10;
    mifare_ultralight_fts.write_block_size = 0x04;
    mifare_ultralight_fts.user_memory_size = (
        mifare_ultralight_fts.end_block_number - 
        mifare_ultralight_fts.start_block_number + 1
    ) * mifare_ultralight_fts.block_size;
    
    // Mifare Standard 1K
    // See http://www.orangetags.com/wp-content/downloads/datasheet/NXP/MIFARE%20MF1%20IC%20S50.pdf
    
    // Mifare Standard 4K
    // See http://www.orangetags.com/wp-content/downloads/datasheet/NXP/MIFARE%20MF1%20IC%20S70.pdf
    
    ///
    ///    The EEPROM diagrams of Mifare Standard 1K/4K are complex. As for now, let us stick to
    ///    the usage of Mifare Ultralight.
    ///
}
 
template<class OutStream>
Picc<OutStream>::~Picc()
{
    if (recv_buffer != nullptr){ free(recv_buffer); recv_buffer = nullptr;}
    if (h_context != 0){SCardReleaseContext(h_context); h_context = 0;}
    if(!used_apdus.empty()) used_apdus.clear();
    if(!received_data.empty()) received_data.clear();
}
   
///
///    @brief Reads data from PICC (Smart Card)
///    
///    Note that standard read APDU is issued. The read process depends on the design
///    of the user memory of the card. The number of bytes to read, given in the
///    @param sz_read defaults to the entire user memory if its value is 0. 
///    Otherwise use it to specify how much data should be read. Upon return, @b sz_read will
///    contain the length of the data read from PICC. So, it must not be nullptr.
    
///    @param data is used to store all the data read from the PICC. It must be
///    explicitly given and unallocated. After use, it should be properly freeed
///    by user by calling @b free().
///
template<class OutStream>
long_t Picc<OutStream>::Read(byte_t *&data, dword_t *sz_read)
{
    long_t rv;
    
    if(sz_read == nullptr)
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Picc<OutStream>::read:invalid_parameter");
        return rv;
    }
    
    // Get ATR and Handle it
    rv = wait_for_reader_and_card();
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::read:wait_for_reader_and_card");
        return rv;
    }
    // Make read APDUs  
    LP_EEPROM_FEATURES features = nullptr;
    
    if(is_mifare_ultralight(NewAtr()))
    {
        features = &mifare_ultralight_fts;
    }
    
    dword_t size = *sz_read;
    rv = make_read_apdus(features, size);
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::read:make_read_apdus");
        return rv;
    } 
    
    rv = transmit_apdus();
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::read:transmit_apdus");
        return rv;
    }
    
    rv = assemble_read_data(data, sz_read);
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::read:assemble_read_data");
        return rv;
    }
    
    return rv;
}

///
/// @briew Writes given data to smart card, aka, PICC
///    
/// Note than standard apdu command for writing is used.
/// According to ACS NFC API, <a href="http://downloads.acs.com.hk/drivers/en/API-ACR122U-2.04.pdf" > here </a>,
/// whose card reader we are writing code for, the APDU format is as follows:
/// <p>
///  ___________________________________________________________
/// |Class|INS|P1|P2          |Lc                   |DataIn
/// |_____|___|__|____________|_____________________|___________
/// | FF  | D6|00|Block Number|Nb of bytes to update|Block data
/// |_____|___|__|____________|_____________________|___________
///    
/// </p>
///    
/// Example: @b FFD60004044F626564 writes @b 0x04 bytes starting from block number @b 0x04,
///          where those bytes are @b 4F626564.
///             
/// Note that the maximum number of bytes to write depends on the type of smart card:
/// 4 bytes for MIFARE Ultralight and 16 bytes for MIFARE 1K/4K.
/// What needs fathoming is the size of a block (page), and the number of blocks (pages) 
/// on the smart card.
///    
/// <a href="http://www.orangetags.com/wp-content/downloads/datasheet/NXP/MF0ICU1.pdf">
/// MIFARE Ultralight </a> has 16 pages, each 4 bytes long. But only 12 pages can be written to.
/// <a href="http://www.orangetags.com/wp-content/downloads/datasheet/NXP/MIFARE%20MF1%20IC%20S50.pdf">
/// MIFARE Classic 1K </a> has 16 sectors with 4 blocks of 16 bytes each (1 block consists of 16 bytes).
/// <a href="http://www.orangetags.com/wp-content/downloads/datasheet/NXP/MIFARE%20MF1%20IC%20S70.pdf">
/// MIFARE Classic 4K </a> has 32 sectors with 4 blocks and 8 sectors with 16 blocks (1 block consists of 16 bytes).
///    
/// See <a href="http://www.orangetags.com/"> here </a> for more info on RFID tags.
///
template<class OutStream>
long_t Picc<OutStream>::Write()
{
    long_t rv;
    buffer_t* data = DataToWrite();
    
    // Validate parameters
    if(nullptr == data)
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Picc<OutStream>::write:data.empty");
        return rv;
    }
    
    // Get ATR and Handle it
    rv = wait_for_reader_and_card();
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::write:wait_for_reader_and_card");
        return rv;
    }
    // Make write APDUs  
    LP_EEPROM_FEATURES features = nullptr;
    
    if(is_mifare_ultralight(NewAtr()))
    {
        features = &mifare_ultralight_fts;
    }
    
    rv = make_write_apdus(features, data->data, data->length);
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::write:make_write_apdus");
        return rv;
    } 
    
    rv = transmit_apdus();
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::write:transmit_apdus");
        return rv;
    }
    
    return rv;
}

///
///    @brief Send customized APDUs to PICC
///
template<class OutStream>
long_t Picc<OutStream>::SendApdus(vector<APDU_DATA> apdus)
{
    long_t rv;
    
    rv = wait_for_reader_and_card();
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::send_apdus:wait_for_reader_and_card");
        return rv;
    }
    
    return transmit_apdus(apdus);
}

/// @brief Wait for reader plugged and card into field
/// It also retrieves ATR.
template<class OutStream>
long_t Picc<OutStream>::wait_for_reader_and_card()
{
    long_t rv;
    
    if(h_context == 0)
    {   
        rv = SCardEstablishContext(SCARD_SCOPE_SYSTEM, nullptr, nullptr, &h_context);
        if (rv != SCARD_S_SUCCESS)
        {
            PcscError::log(rv, "Picc<OutStream>::wait_for_reader_and_card:ScardEstablishContext");
            return rv;
        }
    }
    
    // Free any allocated memory
    if (recv_buffer != nullptr){ free(recv_buffer); recv_buffer = nullptr;}
    
    string ss = "Scanning for card reader ...\n";
    verbose(ss);
        
    rv = scan_for_pcd();
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::wait_for_reader_and_card:scan_for_pcd");
        return rv;
    }
    
    SCARD_READERSTATE rd_state;
    rd_state.szReader = pcd_reader_name.c_str();
    rd_state.dwCurrentState = SCARD_STATE_UNAWARE;
    
    ss = string("Waiting for card insertion on reader: ") +
         string(rd_state.szReader) + string("...\n");
    verbose(ss);
    
    // Wait for card insertion
    do 
    {   
        
        rv = SCardGetStatusChange(h_context, TIMEOUT, &rd_state, 1);
        if (rv == SCARD_E_TIMEOUT)
        {
            rv = SCARD_S_SUCCESS;
        }
        if(rd_state.dwEventState & SCARD_STATE_PRESENT)
        {
            //if(!(rd_state.dwEventState & SCARD_STATE_EXCLUSIVE)){
                break;
            //}
        }
        rd_state.dwCurrentState = rd_state.dwEventState;
        
    } 
    while(rv == SCARD_S_SUCCESS);
    
    string atr;

    for(int i=0; i<MAX_ATR_SIZE; i++)
    {
        char ss[] = {'\0', '\0', '\0'};
        int n = sprintf(ss, "%.2X", rd_state.rgbAtr[i]);
        if(n<0)
        {
            rv = ERROR_FORMATTING_DATA;
            PcscError::log(rv, "Picc<OutStream>::wait_for_reader_and_card:sprintf");
            return rv;
        }
        atr.append(string((const char*)ss));
    }

    NewAtr(atr);

    ss = string("Card inserted (ATR: ") + NewAtr() + string(".\n");
    verbose(ss);
    
    rv = SCARD_S_SUCCESS;
    
    
    return rv;
} 

template<class OutStream>
string Picc<OutStream>::NewAtr()
{
    string s;

    {
        lock_guard<mutex> lg(new_atr_mtx);
        s.assign(new_atr);
    }

    return s;
}

template<class OutStream>
void Picc<OutStream>::NewAtr(string atr)
{
    {
        lock_guard<mutex> lg(new_atr_mtx);
        new_atr.assign(atr);
    }
}

template<class OutStream>
string Picc<OutStream>::OldAtr()
{
    string s;

    {
        lock_guard<mutex> lg(old_atr_mtx);
        s.assign(old_atr);
    }

    return s;
}

template<class OutStream>
void Picc<OutStream>::OldAtr(string atr)
{
    {
        lock_guard<mutex> lg(old_atr_mtx);
        old_atr.assign(atr);
    }
}

template<class OutStream>
buffer_t* Picc<OutStream>::DataToWrite()
{
    buffer_t* dt;

    {
        lock_guard<mutex> lg(data_to_write_mtx);
        dt = data_to_write;
    }

    return dt;
}

template<class OutStream>
void Picc<OutStream>::DataToWrite(buffer_t* dt)
{

    {
        lock_guard<mutex> lg(data_to_write_mtx);
        data_to_write = dt;
    }
}

template<class OutStream>
long_t Picc<OutStream>::scan_for_pcd()
{
    long_t rv;
    
    if(!pcd_reader_name.empty())
    {
        rv = SCARD_S_SUCCESS;
        PcscError::log(rv, "Picc<OutStream>::scan_for_pcd:pcd_reader_name.empty");
        return rv;
    }
    
    // Validate crucial arguments
    if(h_context == 0)
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Picc<OutStream>::scan_for_pcd:h_context invalid.");
        return rv;
    }
    
    // Do we have a Proximity Coupling Device (PCD), i.e., Card Reader?
    SCARD_READERSTATE rd_state;
    
    rd_state.szReader = "\\\\?PnP?\\Notification";
    rd_state.dwCurrentState = SCARD_STATE_UNAWARE;
        
    rv = SCardGetStatusChange(h_context, TIMEOUT, &rd_state, 1);
    if (rd_state.dwEventState & SCARD_STATE_UNKNOWN )
    { // Does not support Plug n Play
        rv = SCARD_E_UNKNOWN_READER;
        PcscError::log(rv, "Picc<OutStream>::scan_for_pcd:SCardGetStatusChange: no PnP");
        plug_n_play = false;
    } 
    else if ((rv != SCARD_S_SUCCESS) && (rv != SCARD_E_TIMEOUT))
    {
        PcscError::log(rv, "Picc<OutStream>::scan_for_pcd:SCardGetStatusChange");
        return rv;
    }
    
    // Now scan for readers. Leave this section when a reader is found.
    get_readers:
    
    LPSTR ptr, msz_readers = nullptr;
    dword_t sz_readers, sz_readers_old, nb_readers;
    rv = SCardListReaders(h_context, nullptr, nullptr, &sz_readers); // So as to allocate buffer
    if(rv != SCARD_E_NO_READERS_AVAILABLE && rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::scan_for_pcd:SCardListReaders");
        return rv;
    }
    
    sz_readers_old = sz_readers;
    
    // Since this section might be executed several times, 
    // check whether memory has been allocated and free it.
    if(msz_readers != nullptr)
    {
        free(msz_readers); msz_readers = nullptr;
    }
    
    msz_readers = (LPSTR)malloc(sizeof(char)*sz_readers);
    if(msz_readers == nullptr)
    {
        rv = SCARD_E_INSUFFICIENT_BUFFER;
        PcscError::log(rv, "Picc<OutStream>::scan_for_pcd:malloc");
        return rv;
    }
    
    *msz_readers = '\0';
    rv = SCardListReaders(h_context, nullptr, msz_readers, &sz_readers);
    
    nb_readers = 0;
    ptr = msz_readers;
    while(*ptr != '\0')
    {
        string ss = string("Reader: ") + string(ptr) + string("\n");
        verbose(ss);
        ptr += strlen(ptr)+1;
        nb_readers += 1;
    }
    
    if(rv == SCARD_E_NO_READERS_AVAILABLE || nb_readers == 0)
    { // Waiting for first reader
        if(plug_n_play)
        {
            rd_state.szReader = "\\\\?PnP?\\Notification";
            rd_state.dwCurrentState = SCARD_STATE_UNAWARE;
            
            do 
            {
                rv = SCardGetStatusChange(h_context, TIMEOUT, &rd_state, 1);
            } 
            while(rv == SCARD_E_TIMEOUT);
        } 
        else 
        {
            rv = SCARD_S_SUCCESS;
            while(rv == SCARD_S_SUCCESS || sz_readers == sz_readers_old)
            {
                
                rv = SCardListReaders(h_context, nullptr, nullptr, &sz_readers);
                if(rv == SCARD_E_NO_READERS_AVAILABLE)
                    rv = SCARD_S_SUCCESS;
            }
        }
        
        goto get_readers;
    }
    
    // At least one reader has been found. Grub only the first one.
    // Note: This program will handle just one reader on the system
    pcd_reader_name.append(msz_readers);
    
    if(msz_readers != nullptr)
    {
        free(msz_readers); msz_readers = nullptr;
    }
    
    rv = SCARD_S_SUCCESS;
    
    return rv;
}

///
/// @brief Transmits a series of APDUs to the smart card.
///    
/// It assumes that the vector @b used_apdus of APDUs has been made. More,
/// the card reader is connected and recognized by the system.
/// The received data is stored in @b received_data vector of Apdu's. The received
/// binaries are in the @bin_data field while the APDU sent is in the @b command
/// field and the hex-stringified version of the reiceived data is in the @b data field.
/// The length of the binary data is in the @b bin_data_length field.
///    
/// FIXME: The received buffer was allocated by us, so think about freeing
///      it after use.
///    
/// The @param apdus defaults to empty vector, otherwise, in the absence
/// of @b used_apdus, it is used.
///
template<class OutStream>
long_t Picc<OutStream>::transmit_apdus(vector<APDU_DATA> apdus)
{
    long_t rv;
    
    if(
        pcd_reader_name.empty() || h_context == 0 ||
        (used_apdus.empty() && apdus.empty())
    ){
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Picc<OutStream>::transmit_apdus:invalid parameter");
        return rv;
    }
    if(used_apdus.empty()) used_apdus = apdus;
    
    SCARDHANDLE h_card;
    dword_t active_protocol;
    dword_t preferred_protocols = SCARD_PROTOCOL_T0|SCARD_PROTOCOL_T1|SCARD_PROTOCOL_RAW;
        
    rv = SCardConnect(
        h_context, pcd_reader_name.c_str(), SCARD_SHARE_EXCLUSIVE, 
        preferred_protocols, &h_card, &active_protocol
    );
    if (rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::transmit_apdus:SCardConnect");
        return rv;
    }
        
    // Grub Protocol Control Information (pci)
    const SCARD_IO_REQUEST *send_pci;
        
    switch(active_protocol)
    {
        case SCARD_PROTOCOL_T0: send_pci = SCARD_PCI_T0; break;
        case SCARD_PROTOCOL_T1: send_pci = SCARD_PCI_T1; break;
        case SCARD_PROTOCOL_RAW: send_pci = SCARD_PCI_RAW; break;
        default: return ERROR_UNKNOWN_PROTOCOL;
    }
    
    // Begin transaction
    rv = SCardBeginTransaction(h_card);
    if (rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::transmit_apdus:SCardBeginTransaction");
        return rv;
    }
    
    // Continue with the rest of the transaction
    vector<APDU_DATA>::iterator p_apdu;
    string cmd;
    byte_t* data;
    Apdu *apdu_o = new Apdu();
    APDU_DATA rcv;
    
    for(p_apdu = used_apdus.begin(); p_apdu != used_apdus.end(); p_apdu+=1)
    {
        
        // Manufacture apdu from APDU_DATA
        cmd = p_apdu->command;
        data = p_apdu->data;
        dword_t length = p_apdu->data_length;
       
        rv = apdu_o->make_apdu(cmd, data, length);
        if (rv != SCARD_S_SUCCESS)
        {
            PcscError::log(rv, "Picc<OutStream>::transmit_apdus:make_apdu");
            return rv;
        }
        
        byte_t *apdu = apdu_o->bytes_data;
        dword_t len_apdu = apdu_o->bytes_data_length;
        
        // Allocate receive buffer
        recv_buffer = (byte_t*)malloc(recv_buffer_len);
        if (nullptr == recv_buffer)
        {
            rv = SCARD_E_INSUFFICIENT_BUFFER;
            PcscError::log(rv, "Picc<OutStream>::transmit_apdus:malloc(recv_buffer)");
            return rv;
        }
    
        // printf("Transmitting APDU: %s ... ", apdu_o->string_data.c_str());
        
        // Transmit APDU
        rv = SCardTransmit(
            h_card, send_pci, apdu, len_apdu, nullptr, recv_buffer, &recv_buffer_len
        );
        if (rv != SCARD_S_SUCCESS)
        {
            PcscError::log(rv, "Picc<OutStream>::transmit_apdus:SCardTransmit");
            return rv;
        }
        
        // Handle error code in response
        dword_t resp_code = get_apdu_error_code(recv_buffer, recv_buffer_len);
        if(resp_code != SCARD_S_OK)
        {
            // Reconnect
            // printf("[\033[31mNO\033[0m]\n");
            rv = SCARD_E_ERROR;
            PcscError::log(
                rv, "Picc<OutStream>::transmit_apdus:SCardTransmit: operation failed."
            );
            return rv;
            
        } else {} //printf("[\033[32mOK\033[0m]\n");
        
        rcv.command.assign(apdu_o->string_data);
        
        rcv.bin_data = (byte_t *)malloc(recv_buffer_len);
        if (nullptr == rcv.bin_data)
        {
            rv = SCARD_E_INSUFFICIENT_BUFFER;
            PcscError::log(rv, "Picc<OutStream>::transmit_apdus:malloc(rcv.bin_data)");
            return rv;
        }
        
        memcpy(rcv.bin_data, recv_buffer, recv_buffer_len);
        free(recv_buffer); recv_buffer = nullptr;
        
        rcv.bin_data_length = recv_buffer_len;
        
        // Hex-stringify received data. Leaving scope Apdu destroys used buffers
        // so make sure you create a new object so that it creates its own
        // internal buffer that it will safely destroy.
        apdu_o->clean(); delete apdu_o; apdu_o = nullptr;
        apdu_o = new Apdu(rcv.bin_data, rcv.bin_data_length);
        rv = apdu_o->bytes_to_hexstring();
        if (rv != SCARD_S_SUCCESS)
        {
            PcscError::log(rv, "Picc<OutStream>::transmit_apdus:bytes_to_hexstring");
            return rv;
        }

        dword_t len = apdu_o->string_data.size();
        unique_ptr<byte_t> ptr(new byte_t [len]);
        memcpy(ptr.get(), apdu_o->string_data.data(), len);

        rcv.data  = ptr.get();
        rcv.data_length = len;
        
        received_data.push_back(rcv);
        
        apdu_o->clean();
    }
    
    if(apdu_o != nullptr) delete apdu_o;
    
    // End transaction
    rv = SCardEndTransaction(h_card, SCARD_LEAVE_CARD);
    if (rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::transmit_apdus:SCardEndTransaction");
        return rv;
    }
        
    // Disconnect from PICC
    rv = SCardDisconnect(h_card, SCARD_UNPOWER_CARD);
    if (rv != SCARD_S_SUCCESS)
    {
        PcscError::log(rv, "Picc<OutStream>::transmit_apdus:SCardDisconnect");
        return rv;
    }
    
    return rv;
}

///
/// @brief Format APDUs needed for a read transaction
///    
/// @param sz_read defaults to 0, so it can be left out freely. If so,
/// it will be equal to the entire user memory size;
///    
/// See @func make_read_write_apdus for details.
///
template<class OutStream>
long_t Picc<OutStream>::make_read_apdus(LP_EEPROM_FEATURES features, dword_t sz_read)
{
    return make_read_write_apdus(features, nullptr, 0, false, sz_read);
}

///
/// @brief Format APDUs needed to complete a write transanction.
///    
/// See @func make_read_write_apdus for details.
///
template<class OutStream>
long_t Picc<OutStream>::make_write_apdus(
    LP_EEPROM_FEATURES features, byte_t *data, dword_t sz_data)
{
    return make_read_write_apdus(features, data, sz_data);
}

///
/// @brief Format successive APDUs needed to complete a read/write transaction
///
/// @param data: Data to write if we are set to write mode.
/// @param size_data: Length of data if we are set to write mode.
/// @param sz_read defaults to 0, so it can be left out freely. If so,
/// it will be equal to the entire user memory size.
/// @param bl_write defaults to true, i.e., write mode. It can also be left out
/// in write mode.
///    
/// The data to read/write may require successive APDUs to complete. So this method
/// manufactures those APDUs according to card memory featues.
///   
/// The APDUs are stored in the instance variable @b used_apdus which is a @b vector<APDU_DATA>
/// variable. 
///
template<class OutStream>
long_t Picc<OutStream>::make_read_write_apdus(
    LP_EEPROM_FEATURES features, byte_t *dta, dword_t size_data,
    bool_t bl_write, dword_t sz_read
){
    long_t rv;
    
    // Validate parameters
    if(features == nullptr || ((nullptr == dta || size_data == 0) && bl_write == true))
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Picc<OutStream>::make_read_write_apdus:invalid_parameter");
        return rv;
    }
    
    dword_t sz_data = bl_write ? size_data : sz_read;
    
    if(features->user_memory_size < sz_data)
    {
        if(bl_write)
        {
            rv = SCARD_E_INSUFFICIENT_BUFFER;
            PcscError::log(rv, "Picc<OutStream>::make_read_write_apdus:scard_memory_size");
            return rv;
        } 
        else 
        {
            sz_data = 0;
        }
    }
    
    dword_t start = features->start_block_number;
    dword_t end = features->end_block_number;
    dword_t sz_block = features->block_size;
    dword_t sz_rw_block = bl_write ? 
                       features->write_block_size : 
                       features->read_block_size;
                       
    // Default to entire user memory in read mode if read size is 0
    if(!bl_write && sz_data == 0)
    {
        sz_data = features->user_memory_size;
    }
                       
    // Do the padding. Writing will fail if the number of bytes to write is less than block_size
    byte_t* data = dta;

    if(nullptr != dta && 0 != size_data)
    {
        dword_t mod = size_data % sz_block;

        dword_t size = size_data;

        if(mod>0)
        {
            dword_t pad = sz_block-mod;
            size = size_data + pad;

            unique_ptr<byte_t> ptr(new byte_t [size]);
            data = ptr.get();

            for(dword_t i=size_data; i<size; i++)
            {
                data[i] = 0;
            }
        }

        sz_data = size;
    }
                       
    dword_t bytes_count = 0,
            inc = sz_rw_block/sz_block;
    
    // Initialize apdus vector
    used_apdus.clear();
    
    for(dword_t i=start; i<=end; i+=inc)
    {
        if(bytes_count >= sz_data) break;
        dword_t sz_rem = sz_data - bytes_count;
        if(sz_rw_block > sz_rem) sz_rw_block = sz_rem;
        
        char ss[5] = {'\0', '\0', '\0', '\0', '\0'};
        int n = sprintf(ss, "%.2lX%.2lX", i, sz_rw_block);
        if(n<0)
        {
            rv = ERROR_FORMATTING_DATA;
            PcscError::log(rv, "Picc<OutStream>::make_read_write_apdus:sprintf");
            return rv;
        }
        
        // Format well the command part and the data part
        string sizes(ss), cmd;
        byte_t* sub_data;
       
        if(bl_write)
        {
            unique_ptr<byte_t> ptr(new byte_t [sz_rw_block]);
            sub_data = ptr.get();
            dword_t j = 0;

            for(dword_t i=bytes_count; i<(bytes_count+sz_rw_block); i++)
            {
                if(j >= sz_rw_block)
                {
                    break;
                }

                sub_data[j] = data[i];
                j += 1;
            }

            cmd = const_write_apdu + sizes;
        } 
        else 
        {
            sub_data = nullptr;
            cmd = const_read_apdu + sizes;
        }
        
        APDU_DATA apdu_object;
        apdu_object.command = cmd;
        apdu_object.data = sub_data;
        apdu_object.data_length = nullptr == sub_data ? 0 : sz_rw_block;

        used_apdus.push_back(apdu_object);
        bytes_count += sz_rw_block;      
    }
    
    rv = SCARD_S_SUCCESS;
    
    return rv;
}
///
/// @brief Reads error code from the binary response from PICC.
///   
/// The algorithm here just takes the last SCARD_ERROR_CODE_LENGTH bytes in the response
/// buffer, invert them, and cast them as a number. The aim is to keep
/// them as they appear reading from left to write in specifications.
///    
/// For example the two code numbers 90 00 will be directly rendered
/// as the hex number 0x9000 
///
template<class OutStream>
dword_t Picc<OutStream>::get_apdu_error_code(byte_t* resp, dword_t resp_len)
{
    dword_t code = 0;
    dword_t e_len = SCARD_ERROR_CODE_LENGTH, jj = 1;
    
    if(
        resp == nullptr || resp_len < e_len ||
        e_len > sizeof(dword_t)
    ) return ERROR_GENERAL_ERROR;
    
    byte_t *i_code = (byte_t*)malloc(e_len);
    if(i_code == nullptr) return ERROR_GENERAL_ERROR;
    
    for(int i=0; i<(int)e_len; i++)
    {
        i_code[i] = *(resp+resp_len-jj);
        jj += 1;
    }
    
    memcpy(&code, i_code, e_len);
    
    free(i_code);
    
    return code;    
}

template<class OutStream>
bool_t Picc<OutStream>::is_mifare_ultralight(string atr)
{
    PiccAtr picc_desc(atr);
    
    long_t rv = picc_desc.find_picc_name_according_to_atr();
    if(rv != SCARD_S_SUCCESS)
    {
        PcscError::log(
            rv, "Picc<OutStream>::is_mifare_ultralight:find_picc_name_according_to_atr"
        );
        return false;
    }
    
    string mifare = "Mifare Ultralight";
    string picc_name = picc_desc.picc_name_description;
    
    string ss = string("PICC name: ") +
                picc_desc.picc_name_description + string("\n");
    verbose(ss);
    
    return Apdu::does_string_occur(picc_name, mifare);
}

///
/// @brief It appends together truncs of read data into assembled data
///   
/// Read data from PICC contains response error codes which must be removed.
/// Plus, data is read in truncs that must be merged together.
///    
/// @param @b data will contain the pointer to the buffer containing assembled
/// data, while @param @b length will contain the length of the total assembled
/// data. @b length must not be nullptr.
///    
/// Note that the method assumes the instance variable @b received_data has been
/// properly filled and is not empty. So this method must be called after any method
/// that fills @b received_data has been called.
///    
/// The caller should take care to free @b data once done using it.
///    
/// Do not worry about the @b bin_data field in @b received_data since they
/// are properly freed. After this method, the @b received_data vector is emptied since
/// its data has been consumed.
///
template<class OutStream>
long_t Picc<OutStream>::assemble_read_data(byte_t *&data, dword_t *length)
{
    long_t rv;
    dword_t e_len = SCARD_ERROR_CODE_LENGTH;
    
    // Validate parameters
    if(length == nullptr || received_data.empty())
    {
        rv = ERROR_INVALID_PARAMETER;
        PcscError::log(rv, "Picc<OutStream>::assemble_read_data:invalid_parameter");
        return rv;    
    }
    
    vector<APDU_DATA>::iterator itr;
    dword_t len = 0, prev_len = 0, net_len = 0;
    
    byte_t *bdata = nullptr;
    for(itr = received_data.begin(); itr != received_data.end(); itr++)
    {
        prev_len = len;
        net_len = (itr->bin_data_length - e_len);
        len += net_len;
        if(nullptr == bdata)
        {
            bdata = (byte_t *)malloc(len);
            if(nullptr == bdata)
            {
                rv = SCARD_E_INSUFFICIENT_BUFFER;
                PcscError::log(rv, "Picc<OutStream>::assemble_read_data:malloc");
                return rv;
            }
        } 
        else 
        {
            byte_t *ptr = (byte_t*)realloc(bdata, len);
            if(nullptr == ptr)
            {
                rv = SCARD_E_INSUFFICIENT_BUFFER;
                PcscError::log(rv, "Picc<OutStream>::assemble_read_data:realloc");
                free(bdata); bdata = nullptr;
                return rv;
            }
            bdata = ptr;
        }
        memcpy(bdata+prev_len, itr->bin_data, net_len);
        free(itr->bin_data); itr->bin_data = nullptr; itr->bin_data_length = 0;
    }
    
    *length = len;
    data = bdata;
    received_data.clear();
    
    rv = SCARD_S_SUCCESS;
    
    return rv;
}

template<class OutStream>
void Picc<OutStream>::verbose(string out)
{
    if(out.empty()) return;

    if(nullptr == OutObject)
    {
        printf("%s", out.c_str());
    } 
    else 
    {
        (*OutObject) << out;
    }
}

template<class OutStream>
void Picc<OutStream>::clean()
{
    if(!received_data.empty())
    {
        for(APDU_DATA d: received_data)
        {
            if(nullptr != d.bin_data) free(d.bin_data);
        }
    
        received_data.clear();
    }
}

#endif
