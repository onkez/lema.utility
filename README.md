# [Lema Utility](https://www.lema.io)

## Lema Console UI

![LemaTui](Screenshots/LemaTui.png)

## Description

Lema Utility implements the reading/writing of NFC tags using NFC tag reader.
The core functions for reading and writing can be found in the **Core** directory.

### The package builds one target:
    
    - A console UI version.
    
### Dependencies:
    
    - pcscd, libpcsclite1, libpcsclite-dev (`sudo apt install pcscd libpcsclite1 libpcsclite-dev` on Linux);
    
    - libncurses5, libncurses5-dev (`sudo apt install libncurses5 libncurses5-dev` on Linux);
    
    - libcdk5, libcdk5-dev (`sudo apt install libcdk5 libcdk5-dev` on Linux);
    
    - boost library (`sudo apt install libboost-all-dev` on Linux)
		- Or build and install from sources (See https://www.boost.org/users/download/)
		- Guidelines can be found at https://www.boost.org/doc/libs/1_69_0/more/getting_started/unix-variants.html#easy-build-and-install

The headers bundled with the *pcsc* packages are in PCSC include folder on the local system.
You need to add **PCSC/** prefix to the include rules in your sources and in the system headers
such as **winscard.h** and **pcsclite.h**, etc. If ever the include files cannot be located by
your compiler, please make those fixes. Same may apply to CDK whose files are in **cdk** include folder.

####	E.g:
		- `#include <PCSC/winscard.h>` instead of `#include <winscard.h>`
		- `#include <cdk/cdk.h>` instead of `#include <cdk.h>`
	
####	NOTES:
		- libboost-all-dev is broken at link time on Linux 18.04.1 (4.15.0-29-generic)
			Make sure you are adding needed link rules like `LDADD = -lboost_filesystem` to Makefile.am
			if you are using autotools. Also add the lib path to environment 
			(E.g Add `export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/lib"` to .bashrc and source it)

Author: [Obed-Edom Nkezabahizi](onkezabahizi@gmail.com) 


